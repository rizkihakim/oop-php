<!DOCTYPE html>
<html lang="en" dir="ltr">
  <head>
    <meta charset="utf-8">
    <title></title>
  </head>
  <body>
    <?php
      require('animal.php');
      require('frog.php');
      require('ape.php');

      $sheep = new Animal("shaun");

      echo $sheep->name . "<br />";
      echo $sheep->legs . "<br />";
      $sheep->get_cold_blooded();
      echo "<br />";

      $sungokong = new Ape("Kera sakti");
      $sungokong -> yell();
      echo "<br />";

      $kodok = new Frog("buduk");
      $kodok->jump();
     ?>
  </body>
</html>
