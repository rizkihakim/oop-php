<!DOCTYPE html>
<html lang="en" dir="ltr">
  <head>
    <meta charset="utf-8">
    <title></title>
  </head>
  <body>
    <?php
      class Animal {
        public $name;
        public $legs = 2;

        public function __construct($name) {
          $this->name = $name;
        }

        public function get_cold_blooded() {
          echo "True";
        }
      }
     ?>
  </body>
</html>
